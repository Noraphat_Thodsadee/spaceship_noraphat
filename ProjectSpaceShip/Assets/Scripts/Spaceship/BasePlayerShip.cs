using UnityEngine;

namespace Spaceship
{
    public abstract class BasePlayerShip : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected MissileBullet defaultMissileBullet;
        [SerializeField] protected Transform gunMissilePosition;

        protected AudioSource audioSource;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }
        
        public MissileBullet MissileBullet { get; private set; }

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(defaultMissileBullet != null, "defaultMissileBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(gunMissilePosition != null, "gunMissilePosition cannot be null");
        }        
        
        protected void Init(int hp, float speed, Bullet bullet,MissileBullet missileBullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
            MissileBullet = missileBullet;
        }

        public abstract void Fire();
        public abstract void Missile();
    }
}