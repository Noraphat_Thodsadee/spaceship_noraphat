﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class Advice : MonoBehaviour
    {
        public void Play()
        {
            SceneManager.LoadScene("Game");
        }
    
        public void Back()
        {
            SceneManager.LoadScene("Menu");
        }
    } 
}
