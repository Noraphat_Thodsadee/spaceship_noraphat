using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MenuManager : MonoBehaviour 
    {
        public void Play()
        {
            SceneManager.LoadScene("Advice");
        }
    
        public void ExitGame()
        {
            Application.Quit();
        }
    
    
    }
}